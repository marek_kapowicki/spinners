
# Spinners APP

My first scala application that takes an incoming order and calculates how many boxes are needed to ship the order, plus how much the shipping cost is.
There is a command-line application that takes as input an order
JSON file and writes to standard output the shipment information.

## Running

Application is started using a shell script called run.sh that accepts a single parameter, the file name of the JSON input for the case

for instance
```
./run.sh src/test/resources/sample_request.json
```
Command executes the application jar
### Running the tests

Application contains the spock tests executed by

```
gw clean build
```

### Deployment

Gradle wrapper is used to build the application jar
```
gw clean awesomeFunJar
```

## Additional info

Using Groovy, Gradle, Spock to test drive a Scala solution.

* using Gradle as build system.
* Spock and Groovy to drive with Data Driven Tests
* Scala to implement the solution using some nifty tricks.

