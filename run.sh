#!/bin/bash
jarFile="build/libs/spinners-standalone.jar"

if ! [ -f "$jarFile" ]; then
  gw clean awesomeFunJar
fi

java -Done-jar.silent=true -Done-jar.verbose=false -jar $jarFile $1