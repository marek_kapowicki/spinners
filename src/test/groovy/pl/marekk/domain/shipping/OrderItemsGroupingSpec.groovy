package pl.marekk.domain.shipping

import scala.collection.Seq
import spock.lang.Specification

import static pl.marekk.infrastracture.CollectionConverter.asJava
import static pl.marekk.infrastracture.CollectionConverter.asSeq

class OrderItemsGroupingSpec extends Specification {
    def 'group one color items'() {
        given:
            OrderItem item1 = new OrderItem('red', 10)
            OrderItem item2 = new OrderItem('red', 20)
            Seq<OrderItem> items = asSeq([item1, item2])
        when:
            Collection<OrderItem> result = asJava(OrderItems.groupBy(items))
        then:
            result.size() == 1
            result.contains(30)
    }

    def 'group few colors items'() {
        given:
            OrderItem redItem1 = new OrderItem('red', 10)
            OrderItem redItem2 = new OrderItem('red', 20)
            OrderItem blueItem2 = new OrderItem('blue', 50)
            OrderItem blackItem1 = new OrderItem('black', 1)
            OrderItem blackItem2 = new OrderItem('black', 2)
            OrderItem blackItem3 = new OrderItem('black', 3)
            Seq<OrderItem> items = asSeq([redItem1, blackItem1, redItem2, blackItem2, blueItem2, blackItem3])
        when:
            Collection<Integer> result = asJava(OrderItems.groupBy(items))
        then:
            result.size() == 3
        and: 'quantity of red items'
            result.contains(30)
        and: 'quantity of blue items'
            result.contains(50)
        and: 'quantity of black items'
            result.contains(6)
    }
}
