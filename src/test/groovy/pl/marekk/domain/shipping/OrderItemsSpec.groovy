package pl.marekk.domain.shipping

import pl.marekk.api.LineItem
import pl.marekk.infrastracture.CollectionConverter
import scala.collection.Seq
import spock.lang.Specification

import static pl.marekk.infrastracture.CollectionConverter.asJava

class OrderItemsSpec extends Specification {

    def 'order items from sample request'() {
        given:
            List<LineItem> lineItems = [new LineItem('10000FF', '10', '200', 'PLN')]
            Seq<LineItem> seq = CollectionConverter.asSeq(lineItems)

        when:
            Collection<OrderItem> orderItems = asJava(OrderItems.apply(seq).items())
        then:
            orderItems.size() == 1
            with(orderItems.iterator().next()) {
                color() == '0000FF'
                quantity() == 10
            }
    }


    def 'exception is thrown during creation order from wron request'() {
        given:
            List<LineItem> lineItems = [new LineItem('10000FF', '-5', '200', 'PLN')]
            Seq<LineItem> seq = CollectionConverter.asSeq(lineItems)
        when:
            OrderItems.apply(seq)
        then:
            thrown(IllegalArgumentException)
    }
}
