package pl.marekk.domain.shipping

import org.junit.Ignore
import pl.marekk.infrastracture.CollectionConverter
import scala.Tuple2
import spock.lang.Specification

@Ignore('run only manually - some scala import issue in gradle')
class ShippingSummarySpec extends Specification {
    def 'calculate shipping price = #price for regular order with boxes = #boxes'() {

        when:
            ShippingSummary shippingSummary = ShippingSummaries.regular(CollectionConverter.asSeq(boxes))
        then:
            shippingSummary.price() == price
            shippingSummary.toString()
        where:
            boxes                                                    || price
            [new Tuple2(500, 1)]                             || 5 + 1
            [new Tuple2(500, 1), new Tuple2(200, 5)] || 5 + 2 * 5 + 6

    }

    def 'calculate shipping price = #price for express order with boxes = #boxes'() {

        when:
            ShippingSummary shippingSummary = ShippingSummaries.express(CollectionConverter.asSeq(boxes))
        then:
            shippingSummary.price() == price
        where:
            boxes                                                    || price
            [new Tuple2(500, 1)]                             || 5 + 1 * 2
            [new Tuple2(500, 1), new Tuple2(200, 5)] || 5 + 2 * 5 + 6 * 2

    }
}