package pl.marekk.domain.shipping

import pl.marekk.api.OrderCreationRequest
import pl.marekk.api.Orders
import spock.lang.Specification

import static pl.marekk.api.Orders.sample
import static pl.marekk.domain.shipping.Priority.*
import static pl.marekk.infrastracture.CollectionConverter.asJava

class ShippingSpec extends Specification {
    def 'create shipping from sample request'() {
        given:
            OrderCreationRequest request = sample().assemble()
        when:
            Shipping shipping = Shipping.apply(request)
        then:
            with(shipping) {
                priority() == REGULAR()
                asJava(order().items()).size() > 0
            }
    }

    def 'create shipping with priority #requestPriority'() {
        given:
            OrderCreationRequest request = Orders.empty()
                    .withPriority(requestPriority).assemble()
        when:
            Shipping shipping = Shipping.apply(request)
        then:
            shipping.priority() == expectedPriority

        where:
            requestPriority || expectedPriority
            'REGULAR'       || REGULAR()
            'EXPRESS'       || EXPRESS()
            'express'       || EXPRESS()
            'Expedited'     || EXPEDITED()
            'other'         || REGULAR()
            null            || REGULAR()

    }

    def 'create shipping item'() {
        given:
            OrderCreationRequest request = Orders.empty()
                    .addItem('2F0F8FF', '10').assemble()
        when:
            Shipping shipping = Shipping.apply(request)
        then:
            with(asJava(shipping.order().items()).iterator().next()) {
                color() == 'F0F8FF'
                quantity() == 10
            }
    }

    def 'create shipping summary for one item spinners and quantity = #quantity'() {
        given:
            OrderCreationRequest request = Orders.empty()
                    .addItem('2F0F8FF', quantity).assemble()
        when:
            ShippingSummary summary = Shipping.apply(request).createSummary()
        then:
            asJava(summary.boxes()) containsAll(expectedBoxNumber)
        where:
            quantity || expectedBoxNumber
            '5'      || [new scala.Tuple2(Box.apply(1, 100), 1)]
            '600'    || [new scala.Tuple2(Box.apply(5, 500), 1), new scala.Tuple2(Box.apply(1, 100), 1)]
    }

    def 'create shipping summary for two items'() {
        given:
            OrderCreationRequest request = Orders.empty()
                    .addItem('2F0F8AA', '50')
                    .addItem('2F0F8FF', '50').assemble()
        when:
            ShippingSummary summary = Shipping.apply(request).createSummary()
        then:
            asJava(summary.boxes()) contains(new scala.Tuple2(Box.apply(1, 100), 2))
    }
}
