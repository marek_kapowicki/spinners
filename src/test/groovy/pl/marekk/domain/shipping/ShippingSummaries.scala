package pl.marekk.domain.shipping

object ShippingSummaries {

  def regular(boxes: Seq[(Int, Int)]): ShippingSummary = {
    ShippingSummary(Priority.REGULAR, boxes)
  }

  def express(boxes: Seq[(Int, Int)]): ShippingSummary = {
    ShippingSummary(Priority.EXPRESS, boxes)
  }
}
