package pl.marekk.api

import pl.marekk.infrastracture.CollectionConverter
import spock.lang.Specification

class OrderCreationSpec extends Specification {


    def 'create order from valid json'() {
        given:
            String jsonRequest = Orders.VALID_REQUEST
        when:
            OrderCreationRequest orderCreationRequest = OrderManagementService.parseJson(jsonRequest)

        then:
            with(orderCreationRequest) {
                id
                customer
                priority() == 'REGULAR'
                items().size() == 3
            }
            CollectionConverter.asJava(orderCreationRequest.items()).each {
                i -> assertItem(i)
            }
    }

    private static void assertItem(LineItem item) {
        assert item.sku()
        assert item.quantity()
        assert item.currency()

    }
}
