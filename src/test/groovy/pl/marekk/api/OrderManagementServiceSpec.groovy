package pl.marekk.api

import spock.lang.Specification

import static pl.marekk.api.Orders.VALID_REQUEST

class OrderManagementServiceSpec extends Specification {
    def "process sample order"() {
        when:
            def result = OrderManagementService.process(VALID_REQUEST)
        then:
            result.contains('Medium boxes: 1')
            result.contains('Large boxes: 1')
            result.contains('Small boxes: 1')
            result.contains('Shipping cost: $11')
    }
}
