package pl.marekk.api

import pl.marekk.infrastracture.CollectionConverter

import static pl.marekk.infrastracture.CollectionConverter.asSeq

class Orders {
    static final String VALID_REQUEST = this.getClass().getResource('/sample_request.json').text

    private int id = 1
    private Customer customer = new Customer('marek', 'kapowicki')
    private String priority
    private Collection<LineItem> lineItems = []

    static Orders sample() {
        return from(OrderManagementService.parseJson(VALID_REQUEST))
    }

    static Orders empty() {
        return new Orders()
    }

    private static Orders from(OrderCreationRequest request) {
        new Orders(request.priority(), CollectionConverter.asJava(request.items()))
    }

    private Orders() {
    }

    Orders(String priority, Collection<LineItem> lineItems = []) {
        this.priority = priority
        this.lineItems = lineItems
    }

    Orders withPriority(String priority) {
        this.priority = priority
        this
    }

    Orders addItem(String sku, String quantity, String price = "100", String currency = 'EUR') {
        this.lineItems.add(new LineItem(sku, quantity, price, currency))
        this
    }


    OrderCreationRequest assemble() {
        return new OrderCreationRequest(id, customer, priority, asSeq(lineItems))
    }


}
