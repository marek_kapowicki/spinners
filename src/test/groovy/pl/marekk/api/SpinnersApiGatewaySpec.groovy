package pl.marekk.api

import spock.lang.Specification

class SpinnersApiGatewaySpec extends Specification {

    def 'process the valid json file'() {
        given:
            String file = 'sample_request.json'
        when:
            String result = SpinnersApiGateway.processForTest(file)
        then:

            result.contains('Medium boxes: 1')
            result.contains('Large boxes: 1')
            result.contains('Small boxes: 1')
            result.contains('Shipping cost: $11')
    }

    def 'process the not existing file'() {
        given:
            String file = 'not existing'
        when:
            def result = SpinnersApiGateway.processForTest(file)
        then:
            result.contains('exception during reading')

    }

    def 'process the invalid json file'() {
        given:
            String file = 'invalid_request.json'
        when:
            def result = SpinnersApiGateway.processForTest(file)
        then:
            result.contains('exception during parsing')

    }
}
