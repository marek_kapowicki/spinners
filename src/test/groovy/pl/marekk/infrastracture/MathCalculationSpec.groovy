package pl.marekk.infrastracture

import scala.collection.immutable.Seq
import spock.lang.Specification

import static pl.marekk.infrastracture.CollectionConverter.asJava
import static pl.marekk.infrastracture.CollectionConverter.asSeq

class MathCalculationSpec extends Specification {

    def "throw exception for non positive number = #number"() {
        when:
            MathCalculation.numberAsProductOf(number, asSeq([100]))
        then:
            thrown(IllegalArgumentException)
        where:
            number << [-5, -1, 0]
    }

    def 'present number #number as product od #products'() {

        when:
            Seq<scala.Tuple2> result = MathCalculation.numberAsProductOf(number, asSeq(products))
        then:
            asJava(result).containsAll(expectedResult)

        where:
            number | products        || expectedResult
            36     | [100]           || [new scala.Tuple2(100, 1)]
            36     | [500, 200, 100] || [new scala.Tuple2(100, 1)]
            100    | [100]           || [new scala.Tuple2(100, 1)]
            406    | [100]           || [new scala.Tuple2(100, 5)]
            500    | [100]           || [new scala.Tuple2(100, 5)]
            500    | [200, 100]      || [new scala.Tuple2(200, 2), new scala.Tuple2(100, 1)]
            1300   | [500, 200, 100] || [new scala.Tuple2(500, 2), new scala.Tuple2(200, 1), new scala.Tuple2(100, 1)]

    }
}
