package pl.marekk.infrastracture

import scala.collection.JavaConverters
import scala.collection.Seq

/**
 * effect of chhosing spock + groovy as a testing framework
 */
class CollectionConverter {

    static <E> Collection<E> asJava(Seq<E> seq) {
        return JavaConverters.asJavaCollection(seq)
    }

    static <E> Seq<E> asSeq(Collection<E> collection) {
        return collection.toScalaSeq()
    }
}
