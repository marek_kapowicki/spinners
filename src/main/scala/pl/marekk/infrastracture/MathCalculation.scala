package pl.marekk.infrastracture

object MathCalculation {
  /**
    * for instance 1300 = [(500,2), (200, 1), (100, 1)]
    * but 1305 = [(500,2), (200, 1), (100, 2)]
    *
    * @param num     number to be represent
    * @param factors factors
    * @return
    */
  def numberAsProductOf(num: Int, factors: Seq[Int]): Seq[(Int, Int)] = {
    if (num <= 0) {
      throw new IllegalArgumentException("number must be positive")
    }
    val result = factors.sortWith(_ > _)
      .foldLeft(Tuple2(num, Seq(Tuple2(0, 0)))) { (x, y) =>
        Tuple2(x._1 % y, x._2.:+(Tuple2(y, x._1 / y)))
      }
    result._1 match {
      case 0 => result._2.drop(1)
      case _ =>
        val last = result._2.last
        result._2.drop(1).init :+ Tuple2(last._1, last._2 + 1)

    }
  }
}
