package pl.marekk

import pl.marekk.api.SpinnersApiGateway

object SpinnersApp extends App {
  if (args.length == 1) {
    println(SpinnersApiGateway.process(args {0}))
  }
  else
    println("no arguments....")
}
