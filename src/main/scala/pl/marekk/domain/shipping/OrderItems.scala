package pl.marekk.domain.shipping {

  import pl.marekk.api.LineItem

  private[shipping] class OrderItems(val items: Seq[OrderItem])

  private[shipping] class OrderItem(val color: String, val quantity: Int) {
    if (quantity <= 0) {
      throw new IllegalArgumentException("quentity must be positive")
    }
  }

  private[shipping] object OrderItems {
    def apply(items: Seq[LineItem]): OrderItems = {
      new OrderItems(items.map(e => new OrderItem(extractColor(e.sku), e.quantity.toInt)))
    }


    private def extractColor(sku: String) = {
      val Pattern = "([1-3][A-Fa-f0-9]{6})".r

      val color = sku match {
        case Pattern(x) => x.substring(1)
        case _ => throw new IllegalArgumentException(" ")
      }
      color.asInstanceOf[String]
    }

    def groupBy(items: Seq[OrderItem]): Seq[Int] = {
      val resultItems = items
        .groupBy(_.color)
        .mapValues(_.reduce((x, y) => new OrderItem(x.color, x.quantity + y.quantity)))
        .valuesIterator.map(_.quantity).toSeq
      resultItems.asInstanceOf[Seq[Int]]
    }
  }

}