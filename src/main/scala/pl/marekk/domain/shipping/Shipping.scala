package pl.marekk.domain.shipping

import pl.marekk.api.OrderCreationRequest
import pl.marekk.domain.shipping.Priority.{Priority, fromName}
import pl.marekk.infrastracture.MathCalculation


private[shipping] class Shipping(private val order: OrderItems, private val priority: Priority) {
  def createSummary(): ShippingSummary = {
    val quantities = OrderItems.groupBy(order.items)
    ShippingSummary(priority,
      quantities.flatMap(e => {
        val boxes = Boxes.all().map(b => b.capacity)
        MathCalculation.numberAsProductOf(e, boxes)
      }))
  }
}

object Shipping {
  def apply(request: OrderCreationRequest): Shipping = {
    new Shipping(OrderItems(request.items), fromName(request.priority))
  }
}

private[shipping] case class Box(price: Int, capacity: Int)

private[shipping] object SMALL extends Box(1, 100) {
  override def toString: String = "Small boxes:"
}

private[shipping] object MEDIUM extends Box(2, 200) {
  override def toString: String = "Medium boxes:"
}

private[shipping] object LARGE extends Box(5, 500) {
  override def toString: String = "Large boxes:"
}

private[shipping] object Boxes {
  def all(): Seq[Box] = {
    Seq(LARGE, MEDIUM, SMALL)
  }

  def fromCapacity(capacity: Int): Box = {
    all().find(b => b.capacity == capacity).orNull
  }
}

private[shipping] object Priority extends Enumeration {
  type Priority = Value
  val REGULAR: Priority.Value = Value(1)
  val EXPRESS: Priority.Value = Value(2)
  val EXPEDITED: Priority.Value = Value(5)


  def fromName(name: String): Value =
    values.find(name != null && _.toString.toLowerCase == name.toLowerCase()).getOrElse(REGULAR)
}



