package pl.marekk.domain.shipping

import pl.marekk.domain.shipping.Boxes.fromCapacity
import pl.marekk.domain.shipping.Priority.Priority


class ShippingSummary(val priority: Priority, val price: Int, val boxes: Seq[Tuple2[Box, Int]]) {
  def boxesToString(): String = {
    boxes.map(b => b._1 + " " + b._2).mkString("\n")
  }

  override def toString: String = boxesToString + "\nShipping cost: $" + price
}

object ShippingSummary {
  def apply(priority: Priority, boxes: Seq[Tuple2[Int, Int]]): ShippingSummary = {
    val groupedBoxes = boxes.groupBy(box => box._1)
      .mapValues(_.reduce((x, y) => Tuple2(x._1, x._2 + y._2))).values.toSeq.sorted
    val boxNumbers = groupedBoxes.map(box => Tuple2(fromCapacity(box._1), box._2)).filter(it => it._2 > 0)

    val price = boxNumbers.map(b => (b._1.price + priority.id) * b._2).sum
    new ShippingSummary(priority, price, boxNumbers)
  }
}


