package pl.marekk.api

import pl.marekk.domain.shipping.Shipping
import play.api.libs.json.Json

import scala.util.{Failure, Success, Try}

private [api] object OrderManagementService {
  def process(jsonString: String): String = {
    val request: OrderCreationRequest = parseJson(jsonString)
    Try(Shipping(request)) match {
      case Success(shipping) => shipping.createSummary().toString
      case Failure(f) => "exception during parsing file: " + f.getMessage
    }
  }

  def parseJson(jsonString: String): OrderCreationRequest = {
    val orderCreationRequest = Json.parse(jsonString)
    orderCreationRequest.as[OrderCreationRequest]
  }

}
