package pl.marekk.api

import play.api.libs.json.{JsPath, Reads}

case class OrderCreationRequest(id: Int,
                                customer: Customer,
                                priority: String,
                                items: Seq[LineItem])

private [api] object OrderCreationRequest {

  import play.api.libs.functional.syntax._
  import play.api.libs.json._

  implicit val orderReads: Reads[OrderCreationRequest] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "customer").read[Customer] and
      (JsPath \ "shipping_priority").read[String] and
      (JsPath \ "line_items").read[Seq[LineItem]]
    ) (OrderCreationRequest.apply _)
}

case class Customer(firstname: String, lastname: String)

object Customer {

  import play.api.libs.functional.syntax._
  import play.api.libs.json._

  implicit val customerReads: Reads[Customer] = (
    (JsPath \ "firstname").read[String] and
      (JsPath \ "lastname").read[String]
    ) (Customer.apply _)


}

case class LineItem(sku: String, quantity: String, price: String, currency: String)

import play.api.libs.functional.syntax._


object LineItem {
  implicit val itemReads: Reads[LineItem] = (
    (JsPath \ "sku").read[String] and
      (JsPath \ "quantity").read[String] and
      (JsPath \ "price").read[String] and
      (JsPath \ "currency").read[String]

    ) (LineItem.apply _)
}