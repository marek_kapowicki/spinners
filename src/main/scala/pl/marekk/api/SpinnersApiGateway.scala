package pl.marekk.api

import scala.io.Source
import scala.util.{Failure, Success, Try}

object SpinnersApiGateway {

  def process(filename: String): String = {
    process(readTextFile(filename))
  }


  def processForTest(filename: String): String = {
    process(readResourceFile(filename))
  }

  private def process(content: Try[List[String]]): String = {
    content match {
      case Success(lines) => OrderManagementService.process(lines.mkString("\n"))
      case Failure(f) => "exception during reading file: " + f.getMessage
    }
  }

  private def readTextFile(filename: String): Try[List[String]] = {
    Try(Source.fromFile(filename).getLines.toList)
  }

  private def readResourceFile(filename: String): Try[List[String]] = {
    Try(Source.fromResource(filename).getLines.toList)
  }
}
